#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "VikingsOnline/Weather/EPartOfDay.h"
#include "VikingsOnline/Weather/ESeasons.h"
#include "VikingsOnline/Weather/EWeather.h"
#include "VikingsOnline/Weather/Season.h"
#include "VikingsGameStateBase.generated.h"

#define START_HOUR_OF_DAY 7.f
#define YEAR_LENGTH 365
/**
 * Main state of the game
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangePartOfDay, EPartOfDay, PartOfDay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeDay, int32, Day);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeSeason, ESeasons, Season);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeWeather, EWeather, Weather);

UCLASS()
class VIKINGSONLINE_API AVikingsGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	const float GameDayLength{24.f};

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	float DayLength{300.f};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	float CurrentTimeOfDay{START_HOUR_OF_DAY};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	int32 CurrentDay{0};
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	int32 CurrentDayOfYear{0};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	float StartHourOfDay{START_HOUR_OF_DAY};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	EPartOfDay PartOfDay{EPartOfDay::MORNGING};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	TMap<ESeasons, FSeason> Seasons{};

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Environment)
	FSeason CurrentSeason;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category=Weather)
	EWeather CurrentWeather{EWeather::SUNNY};
	
	UPROPERTY()
	FOnChangePartOfDay OnChangePartOfDayDelegate;

	UPROPERTY()
	FOnChangeDay OnChangeDayDelegate;

	UPROPERTY()
	FOnChangeWeather OnChangeWeatherDelegate;

	UPROPERTY()
	FOnChangeSeason OnChangeSeasonDelegate;
	
	AVikingsGameStateBase();
	
	virtual void Tick(float DeltaSeconds) override;
	
private:
	void RefreshCurrentTimeOfDay(const float DeltaSeconds);
	void RefreshPartOfDay();
	void RefreshDay();
	void RefreshWeather();
	void RefreshSeason();
	bool SwitchSeason(const ESeasons Type, const FSeason& SeasonData) const
	{
		return CurrentDayOfYear >= SeasonData.StartDayOfYear && CurrentDayOfYear <= SeasonData.EndDayOfYear && Type != CurrentSeason.SeasonType;
	}
};
