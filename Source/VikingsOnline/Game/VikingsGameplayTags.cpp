﻿#include "VikingsGameplayTags.h"

UE_DEFINE_GAMEPLAY_TAG(Sword1h, TEXT("Als.OverlayMode.Sword1h"));
UE_DEFINE_GAMEPLAY_TAG(Weapon2h, TEXT("Als.OverlayMode.Weapon2h"));
UE_DEFINE_GAMEPLAY_TAG(Pickaxe, TEXT("Als.OverlayMode.Pickaxe"));

namespace AlsLocomotionActionTags
{
	UE_DEFINE_GAMEPLAY_TAG(Interaction, TEXT("Als.LocomotionAction.Interaction"));
}