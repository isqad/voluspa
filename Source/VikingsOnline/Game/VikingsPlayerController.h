// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "VikingsPlayerController.generated.h"

class UInputAction;
class UInputMappingContext;
struct FInputActionValue;
class AVikingsBaseCharacter;

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnInputAction_Move, const FInputActionValue&, ActionValue);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnInputAction_Jump, const FInputActionValue&, ActionValue);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnInputAction_Scroll, const FInputActionValue&, ActionValue);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnInputAction_Inventory, const FInputActionValue&, ActionValue);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnInputAction_Interact, const FHitResult&, HitResultUnderCursor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputAction_WheelUp);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputAction_WheelDown);

// For debug purposes
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputAction_OverlayMenu);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputAction_OverlayMenuSelected);

UCLASS()
class VIKINGSONLINE_API AVikingsPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category=Interaction)
	FHitResult HitResultUnderCursor;
	
	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY()
	FOnInputAction_Move OnInputActionMoveDelegate;
	UPROPERTY()
	FOnInputAction_Jump OnInputActionJumpDelegate;
	UPROPERTY()
	FOnInputAction_Scroll OnInputActionScrollDelegate;
	UPROPERTY()
	FOnInputAction_Inventory OnInputActionInventoryDelegate;
	UPROPERTY()
	FOnInputAction_Interact OnInputActionInteractDelegate;
	
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnInputAction_OverlayMenu OnInputActionOverlayMenuDelegate;
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnInputAction_OverlayMenuSelected OnInputActionOverlayMenuSelectedDelegate;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnInputAction_WheelDown OnInputActionWheelDownDelegate;
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnInputAction_WheelUp OnInputActionWheelUpDelegate;

	UFUNCTION(BlueprintCallable, Category=Interaction)
	void RotateCharacterToAim(const float DeltaTime);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputMappingContext> InputMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> InventoryAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> ScrollAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> InteractAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> WheelUpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> WheelDownAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	float AimInterpRotationSpeed{0.8};
private:
	void InputAction_Inventory(const FInputActionValue& ActionValue);
	void InputAction_Scroll(const FInputActionValue& ActionValue);
	void InputAction_Move(const FInputActionValue& ActionValue);
	void InputAction_Jump(const FInputActionValue& ActionValue);
	void InputAction_Interact(const FInputActionValue& ActionValue);

	void InputAction_WheelUp(const FInputActionValue& ActionValue);
	void InputAction_WheelDown(const FInputActionValue& ActionValue);
	
	void RefreshHitResultUnderCursor();
	
	void RefreshAimRotation(const float DeltaTime);

	float PrevRotationYaw;

#pragma region Debug
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input|Debug", Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> OverlayMenuAction;
	void InputAction_OverlayMenu(const FInputActionValue& ActionValue);
#pragma endregion Debug
	UPROPERTY()
	AVikingsBaseCharacter* PlayerCharacter;
};
