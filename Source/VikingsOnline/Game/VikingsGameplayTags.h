﻿#pragma once

#include "NativeGameplayTags.h"

VIKINGSONLINE_API UE_DECLARE_GAMEPLAY_TAG_EXTERN(Sword1h);
VIKINGSONLINE_API UE_DECLARE_GAMEPLAY_TAG_EXTERN(Weapon2h);
VIKINGSONLINE_API UE_DECLARE_GAMEPLAY_TAG_EXTERN(Pickaxe);

namespace AlsLocomotionActionTags
{
	VIKINGSONLINE_API UE_DECLARE_GAMEPLAY_TAG_EXTERN(Interaction);
}