// Fill out your copyright notice in the Description page of Project Settings.


#include "VikingsPlayerController.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "DrawDebugHelpers.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "VikingsOnline/Characters/VikingsBaseCharacter.h"

void AVikingsPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (UEnhancedInputComponent* EnhancedInput = Cast<UEnhancedInputComponent>(InputComponent); IsLocalController() && IsValid(EnhancedInput))
	{
		check(IsValid(InventoryAction));
		check(IsValid(MoveAction));
		check(IsValid(JumpAction));
		check(IsValid(ScrollAction));
		check(IsValid(OverlayMenuAction));
		check(IsValid(InteractAction));

		check(IsValid(WheelUpAction));
		check(IsValid(WheelDownAction));
		
		EnhancedInput->BindAction(InventoryAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_Inventory);
		EnhancedInput->BindAction(InteractAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_Interact);
		EnhancedInput->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_Move);
		EnhancedInput->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_Jump);
		EnhancedInput->BindAction(ScrollAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_Scroll);

		EnhancedInput->BindAction(WheelUpAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_WheelUp);
		EnhancedInput->BindAction(WheelDownAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_WheelDown);

		EnhancedInput->BindAction(OverlayMenuAction, ETriggerEvent::Triggered, this, &ThisClass::InputAction_OverlayMenu);
	}
}

void AVikingsPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (const auto LocalPlayer = GetLocalPlayer(); IsLocalController() && LocalPlayer)
	{		
		bShowMouseCursor = true;

		if (const auto InputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()); InputSubsystem)
		{
			InputSubsystem->AddMappingContext(InputMappingContext, 0);
		}

		if (IsValid(GetPawn()))
		{
			PlayerCharacter = Cast<AVikingsBaseCharacter>(GetPawn());
		}
		
	}
}

#pragma region Tick
void AVikingsPlayerController::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	RefreshHitResultUnderCursor();
	RefreshAimRotation(DeltaSeconds);
}

void AVikingsPlayerController::RefreshHitResultUnderCursor()
{
	if (IsLocalController())
	{
		GetHitResultUnderCursor(ECC_Visibility, true, HitResultUnderCursor);
	}
}

void AVikingsPlayerController::RefreshAimRotation(const float DeltaTime)
{
	if (IsLocalController() && IsValid(PlayerCharacter))
	{
		const auto ActorLoc = PlayerCharacter->GetActorLocation();

		const auto CharacterHalfHeight = PlayerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	
		DrawDebugSphere(GetWorld(), HitResultUnderCursor.Location, 6.f, 6, FColor::Red);
		DrawDebugLine(GetWorld(), FVector(ActorLoc.X, ActorLoc.Y, CharacterHalfHeight), HitResultUnderCursor.Location, FColor::Green);

		// TODO: вращать головой за курсором
		RotateCharacterToAim(DeltaTime);
	}
}

void AVikingsPlayerController::RotateCharacterToAim(const float DeltaTime)
{
	if (IsValid(PlayerCharacter) && PlayerCharacter->GetDesiredRotationMode() == AlsRotationModeTags::Aiming && !PlayerCharacter->bRotationLocked)
	{
		const auto ViewCursorLoc = HitResultUnderCursor.Location;
		const auto ControlRotationYaw = (ViewCursorLoc - PlayerCharacter->GetActorLocation()).Rotation().Yaw;
		// UKismetMathLibrary::FInterpTo(
		// 	PrevRotationYaw,
		// 	(ViewCursorLoc - PlayerCharacter->GetActorLocation()).Rotation().Yaw,
		// 	DeltaTime,
		// 	20.f
		// 	);
		
		SetControlRotation(FRotator(0.f, ControlRotationYaw, 0.f));

		PrevRotationYaw = ControlRotationYaw;
	}
}
#pragma endregion Tick

void AVikingsPlayerController::InputAction_Inventory(const FInputActionValue& ActionValue)
{
	OnInputActionInventoryDelegate.ExecuteIfBound(ActionValue);
}

void AVikingsPlayerController::InputAction_Scroll(const FInputActionValue& ActionValue)
{
	OnInputActionScrollDelegate.ExecuteIfBound(ActionValue);
}

void AVikingsPlayerController::InputAction_Move(const FInputActionValue& ActionValue)
{
	OnInputActionMoveDelegate.ExecuteIfBound(ActionValue);
}

void AVikingsPlayerController::InputAction_Jump(const FInputActionValue& ActionValue)
{
	OnInputActionJumpDelegate.ExecuteIfBound(ActionValue);
}

void AVikingsPlayerController::InputAction_Interact(const FInputActionValue& ActionValue)
{
	OnInputActionInteractDelegate.ExecuteIfBound(HitResultUnderCursor);
}

void AVikingsPlayerController::InputAction_WheelDown(const FInputActionValue& ActionValue)
{
	OnInputActionWheelDownDelegate.Broadcast();
}

void AVikingsPlayerController::InputAction_WheelUp(const FInputActionValue& ActionValue)
{
	OnInputActionWheelUpDelegate.Broadcast();
}

void AVikingsPlayerController::InputAction_OverlayMenu(const FInputActionValue& ActionValue)
{
	OnInputActionOverlayMenuDelegate.Broadcast();
	
	if (!ActionValue.Get<bool>()) // On release the button
	{
		OnInputActionOverlayMenuSelectedDelegate.Broadcast();
	}
}


