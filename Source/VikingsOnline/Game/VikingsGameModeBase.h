// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VikingsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VIKINGSONLINE_API AVikingsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
