// Fill out your copyright notice in the Description page of Project Settings.


#include "VikingsGameStateBase.h"
#include "VikingsOnline/Weather/Season.h"
#include "Net/UnrealNetwork.h"

AVikingsGameStateBase::AVikingsGameStateBase()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	TMap<EWeather, int32> AutumnWeathers{};
	AutumnWeathers.Add(EWeather::RAIN, 45);
	AutumnWeathers.Add(EWeather::HEAVY_RAIN, 10);
	AutumnWeathers.Add(EWeather::SNOW, 5);
	AutumnWeathers.Add(EWeather::BLIZZARD, 1);
	FSeason Autumn{ESeasons::AUTUMN, 167, 242, AutumnWeathers};

	TMap<EWeather, int32> WinterWeathers{};
	WinterWeathers.Add(EWeather::RAIN, 0);
	WinterWeathers.Add(EWeather::HEAVY_RAIN, 0);
	WinterWeathers.Add(EWeather::SNOW, 45);
	WinterWeathers.Add(EWeather::BLIZZARD, 30);
	FSeason Winter{ESeasons::WINTER, 243, 365, WinterWeathers};

	TMap<EWeather, int32> SpringWeathers{};
	SpringWeathers.Add(EWeather::RAIN, 0);
	SpringWeathers.Add(EWeather::HEAVY_RAIN, 0);
	SpringWeathers.Add(EWeather::SNOW, 45);
	SpringWeathers.Add(EWeather::BLIZZARD, 30);
	FSeason Spring{ESeasons::SPRING, 0, 76, SpringWeathers};

	TMap<EWeather, int32> SummerWeathers{};
	SummerWeathers.Add(EWeather::RAIN, 30);
	SummerWeathers.Add(EWeather::HEAVY_RAIN, 25);
	SummerWeathers.Add(EWeather::SNOW, 0);
	SummerWeathers.Add(EWeather::BLIZZARD, 0);
	FSeason Summer{ESeasons::SUMMER, 0, 166, SummerWeathers};

	Seasons.Add(ESeasons::SUMMER, Summer);
	Seasons.Add(ESeasons::SPRING, Spring);
	Seasons.Add(ESeasons::AUTUMN, Autumn);
	Seasons.Add(ESeasons::WINTER, Winter);

	CurrentSeason = Summer;
}

void AVikingsGameStateBase::BeginPlay()
{
	Super::BeginPlay();

	PartOfDay = EPartOfDay::MORNGING;
	CurrentTimeOfDay = StartHourOfDay;
	CurrentWeather = EWeather::SUNNY;

	OnChangeWeatherDelegate.Broadcast(CurrentWeather);
	OnChangePartOfDayDelegate.Broadcast(PartOfDay);
}

void AVikingsGameStateBase::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	RefreshCurrentTimeOfDay(DeltaSeconds);
	RefreshPartOfDay();
	RefreshDay();
	RefreshWeather();
}

void AVikingsGameStateBase::RefreshCurrentTimeOfDay(const float DeltaSeconds)
{
	CurrentTimeOfDay += (DeltaSeconds / DayLength) * GameDayLength;
	if (CurrentTimeOfDay >= GameDayLength)
	{
		CurrentTimeOfDay -= GameDayLength;
	}
}

void AVikingsGameStateBase::RefreshPartOfDay()
{
	const auto PartOfDayWas = PartOfDay;
	
	if (CurrentTimeOfDay >= 0.f && CurrentTimeOfDay < 6.f)
	{
		PartOfDay = EPartOfDay::NIGHT;
	}
	else if (CurrentTimeOfDay >= 6.f && CurrentTimeOfDay < 12.f)
	{
		PartOfDay = EPartOfDay::MORNGING;
	}
	else if (CurrentTimeOfDay >= 12.f && CurrentTimeOfDay < 16.f)
	{
		PartOfDay = EPartOfDay::DAY;
	}
	else if (CurrentTimeOfDay >= 16.f && CurrentTimeOfDay < 24.f)
	{
		PartOfDay = EPartOfDay::EVENING;
	}

	if (PartOfDay != PartOfDayWas)
	{
		OnChangePartOfDayDelegate.Broadcast(PartOfDay);
	}
}

void AVikingsGameStateBase::RefreshDay()
{
	if (CurrentTimeOfDay >= StartHourOfDay)
	{
		CurrentDay++;
		
		CurrentDayOfYear = CurrentDay;
		if(CurrentDayOfYear >= YEAR_LENGTH)
		{
			CurrentDayOfYear -= YEAR_LENGTH;
		}
		RefreshSeason();
		OnChangeDayDelegate.Broadcast(CurrentDay);
	}
}

void AVikingsGameStateBase::RefreshWeather()
{
}

void AVikingsGameStateBase::RefreshSeason()
{	
	for (const auto& SeasonData : Seasons)
	{
		if (SwitchSeason(SeasonData.Key, SeasonData.Value))
		{
			CurrentSeason = SeasonData.Value;
			OnChangeSeasonDelegate.Broadcast(CurrentSeason.SeasonType);
			break;
		}
	}
}
