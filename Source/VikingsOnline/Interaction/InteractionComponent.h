#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractionComponent.generated.h"

class UCameraComponent;

// Interaction with world (items, buildings...)
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VIKINGSONLINE_API UInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UInteractionComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
protected:
	virtual void BeginPlay() override;

	// Whether interaction mode active or not
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State")
	bool bActive{ false };

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
    float TraceDistance{ 500.f };

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
    float TraceSphereRadius{ 5.f };

private:
    void TraceSphere(FHitResult& OutHit) const;
};
