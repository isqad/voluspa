
#include "InteractionComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/EngineTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/PlayerCameraManager.h"

#define COLLISION_INTERACTIVE_ITEM		ECollisionChannel::ECC_GameTraceChannel1

UInteractionComponent::UInteractionComponent()
{
    // Needed for tracing
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    FHitResult HitResult{};
    TraceSphere(HitResult);
}

void UInteractionComponent::TraceSphere(FHitResult& OutHit) const
{
    if (GetWorld() != nullptr && GetOwner() != nullptr)
    {      
        const APlayerCameraManager* PlayerCamManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
        if (PlayerCamManager != nullptr)
        {
            TArray<AActor*> ActorsToIgnore{};
            ActorsToIgnore.Add(GetOwner());

            const FVector TraceStart = PlayerCamManager->GetCameraLocation();
            const FVector CamForwardVec = UKismetMathLibrary::GetForwardVector(PlayerCamManager->GetCameraRotation());
            const FVector TraceEnd = CamForwardVec * TraceDistance + TraceStart;

            UKismetSystemLibrary::SphereTraceSingle(
                GetWorld(), 
                TraceStart, 
                TraceEnd, 
                TraceSphereRadius,
                UEngineTypes::ConvertToTraceType(COLLISION_INTERACTIVE_ITEM),
                false,
                ActorsToIgnore,
                EDrawDebugTrace::None,
                OutHit,
                true
            );
        }        
    }
}
