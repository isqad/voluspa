#pragma once

#include "InteractionHUDInterface.generated.h"

UINTERFACE(BlueprintType)
class VIKINGSONLINE_API UInteractionHUDInterface : public UInterface 
{
    GENERATED_BODY()
};

class IInteractionHUDInterface
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void ShowInteractionWidget(const FName WidgetText);
    
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void CloseInteractionWidget();
};
