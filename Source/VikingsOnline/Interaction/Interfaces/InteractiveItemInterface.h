#pragma once

#include "InteractiveItemInterface.generated.h"

UINTERFACE(BlueprintType)
class VIKINGSONLINE_API UInteractiveItemInterface : public UInterface 
{
    GENERATED_BODY()
};

class IInteractiveItemInterface
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    FName GetUIInteractText();
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void Interact();
};
