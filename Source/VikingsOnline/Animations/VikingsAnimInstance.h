// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "VikingsAnimInstance.generated.h"

class UCurveFloat;

/**
 * Base class for ABP
 */
UCLASS(Blueprintable)
class VIKINGSONLINE_API UVikingsAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintPure, Category=References, meta=(BlueprintThreadSafe))
	ABaseHuman* TryGetCharacter();

	UFUNCTION(BlueprintPure, Category=CharacterInfo, meta=(BlueprintThreadSafe))
	FCharacterAnimEssentialInfo TryGetAnimEssentialInfo();

	UFUNCTION(BlueprintPure, Category=Utils, meta=(BlueprintThreadSafe))
	static float GetCurveFloatValueThreadSafe(const UCurveFloat* Curve, const float& InTime);
private:
	const double FallSpeedForPredictLand{ 200.0 };
	
	UPROPERTY()
	ABaseHuman* Character;
};
