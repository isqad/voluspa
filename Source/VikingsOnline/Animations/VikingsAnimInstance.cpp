// Fill out your copyright notice in the Description page of Project Settings.


#include "VikingsAnimInstance.h"
#include <VikingsOnline/Characters/BaseHuman.h>
#include "Curves/CurveFloat.h"

ABaseHuman* UVikingsAnimInstance::TryGetCharacter()
{
	if (Character != nullptr)
	{
		return Character;
	}
	
	if (APawn* OwnerPawn = TryGetPawnOwner(); OwnerPawn != nullptr)
	{
		Character = Cast<ABaseHuman>(OwnerPawn);
	}

	return Character;
}

FCharacterAnimEssentialInfo UVikingsAnimInstance::TryGetAnimEssentialInfo()
{
	if (const auto AnimCharacter = TryGetCharacter(); AnimCharacter)
	{
		return AnimCharacter->EssentialMovementValues;
	}

	return FCharacterAnimEssentialInfo{};
}

float UVikingsAnimInstance::GetCurveFloatValueThreadSafe(const UCurveFloat* Curve, const float& InTime)
{
	if (Curve == nullptr)
	{
		return 0.0;
	}

	return Curve->GetFloatValue(InTime);
}
