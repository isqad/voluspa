#pragma once

#include "PlayerHUDInterface.generated.h"

class AHUD;

UINTERFACE(BlueprintType)
class VIKINGSONLINE_API UPlayerHUDInterface : public UInterface 
{
    GENERATED_BODY()
};

class IPlayerHUDInterface
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    AHUD* GetHUD();
};
