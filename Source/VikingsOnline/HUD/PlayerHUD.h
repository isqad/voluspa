// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/IntPoint.h"
#include "GameFramework/HUD.h"
#include "GameplayTagContainer.h"
#include <VikingsOnline/Interaction/Interfaces/InteractionHUDInterface.h>
#include "PlayerHUD.generated.h"

/**
 * Main Player HUD
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOverlayModeSelected, const FGameplayTag&, OverlayMode);

UCLASS()
class VIKINGSONLINE_API APlayerHUD : public AHUD, public IInteractionHUDInterface
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable)
	FOnOverlayModeSelected OnOverlayModeSelectedDelegate;
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnInventoryLoad(const FIntPoint& Dimensions);

	UFUNCTION(BlueprintImplementableEvent)
	void OnInventoryOpen();
};
