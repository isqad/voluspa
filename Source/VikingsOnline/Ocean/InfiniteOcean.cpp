// Fill out your copyright notice in the Description page of Project Settings.


#include "InfiniteOcean.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// Sets default values
AInfiniteOcean::AInfiniteOcean()
{
	PrimaryActorTick.bCanEverTick = false;

	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	check(RootSceneComponent != nullptr);
	SetRootComponent(RootSceneComponent);

	HISMeshComponent = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("HISMeshComponent"));
	check(HISMeshComponent != nullptr);

	HISMeshComponent->SetupAttachment(RootSceneComponent);
}

// Called when the game starts or when spawned
void AInfiniteOcean::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInfiniteOcean::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AInfiniteOcean::Tiling(float GridSize, int32 GridTiles, float CellSize)
{
	check(SurfaceMaterial != nullptr);

	HISMeshComponent->ClearInstances();

	const float TileSize = GridSize / (float)GridTiles;
	const float TilesCenter = (float)GridTiles / 2.f;

	const FVector VHalf(0.5f, 0.5f, 0.0f);
	const FVector VActorLoc(GetActorLocation());
	const FVector VTileSize(TileSize / CellSize);

	for (int32 _y = 0; _y < GridTiles; ++_y)
	{
		const float TileCenterY = (float)_y - TilesCenter;

		for (int32 _x = 0; _x < GridTiles; ++_x)
		{
			const float TileCenterX = (float)_x - TilesCenter;
			FVector VTileCenter(TileCenterX, TileCenterY, 0.f);

			VTileCenter *= TileSize;
			VTileCenter += VHalf * TileSize;
			VTileCenter += VActorLoc;

			const auto Index = HISMeshComponent->AddInstance(FTransform(FRotator::ZeroRotator, VTileCenter, VTileSize));
			HISMeshComponent->SetMaterial(Index, SurfaceMaterial);
		}
	}
}
