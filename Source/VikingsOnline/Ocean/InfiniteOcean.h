// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InfiniteOcean.generated.h"

class UHierarchicalInstancedStaticMeshComponent;
class UMaterialInterface;

UCLASS()
class VIKINGSONLINE_API AInfiniteOcean : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	UMaterialInterface* SurfaceMaterial;

	// Sets default values for this actor's properties
	AInfiniteOcean();
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Tiling(float GridSize = 10000.f, int32 GridTiles = 16, float CellSize = 250.f);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	

private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* RootSceneComponent;

	UPROPERTY(VisibleAnywhere)
	UHierarchicalInstancedStaticMeshComponent* HISMeshComponent;
};
