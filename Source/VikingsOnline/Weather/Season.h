﻿#pragma once

#include "VikingsOnline/Weather/ESeasons.h"
#include "VikingsOnline/Weather/EWeather.h"
#include "Season.generated.h"

USTRUCT(BlueprintType)
struct VIKINGSONLINE_API FSeason
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	ESeasons SeasonType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 StartDayOfYear;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 EndDayOfYear;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<EWeather, int32> WeatherProbabilities{};
};
