// Fill out your copyright notice in the Description page of Project Settings.


#include "Sky.h"
#include "Components/SkyLightComponent.h"
#include "Components/DirectionalLightComponent.h"
#include "../Game/VikingsGameStateBase.h"

ASky::ASky()
{	
	bReplicates = true;

	DirectionalLight = CreateDefaultSubobject<UDirectionalLightComponent>("DirectionalLight");
	SkyLight = CreateDefaultSubobject<USkyLightComponent>("SkyLight");

	DirectionalLight->SetupAttachment(GetRootComponent());
	SkyLight->SetupAttachment(GetRootComponent());

	DirectionalLight->bUseTemperature = true;
}

void ASky::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld())
	{
		if(const auto GState = GetWorld()->GetGameState<AVikingsGameStateBase>(); GState)
		{
			CurrentGameState = GState;

			CurrentGameState->OnChangePartOfDayDelegate.AddDynamic(this, &ThisClass::OnChangePartOfDay);
			CurrentGameState->OnChangeDayDelegate.AddDynamic(this, &ThisClass::OnChangeDay);
		}
	}
}