﻿#pragma once

#include "ESeasons.generated.h"

UENUM(BlueprintType)
enum class ESeasons : uint8
{
	SUMMER = 0 UMETA(DisplayName = "Summer"),
	AUTUMN = 1 UMETA(DisplayName = "Autumn"),
	WINTER = 2 UMETA(DisplayName = "Winter"),
	SPRING = 3 UMETA(DisplayName = "Spring"),
};
