﻿#pragma once

#include "EWeather.generated.h"

UENUM(BlueprintType)
enum class EWeather : uint8
{
	SUNNY = 0 UMETA(DisplayName = "Sunny"),
	RAIN = 1 UMETA(DisplayName = "Rain"),
	HEAVY_RAIN = 2 UMETA(DisplayName = "Heavy Rain"),
	SNOW = 3 UMETA(DisplayName = "Snow"),
	BLIZZARD = 4 UMETA(DisplayName = "Blizzard"),
};
