﻿#pragma once

#include "EPartOfDay.generated.h"

UENUM(BlueprintType)
enum class EPartOfDay : uint8
{
	NIGHT = 0 UMETA(DisplayName = "Night"),
	DAY = 1 UMETA(DisplayName = "Day"),
	MORNGING = 2 UMETA(DisplayName = "Morning"),
	EVENING = 3 UMETA(DisplayName = "Evening"),
};
