// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sky.generated.h"

class UDirectionalLightComponent;
class USkyLightComponent;
class AVikingsGameStateBase;

UCLASS()
class VIKINGSONLINE_API ASky : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASky();
	
protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TObjectPtr<UDirectionalLightComponent> DirectionalLight;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TObjectPtr<USkyLightComponent> SkyLight;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TMap<EPartOfDay, float> DirectionalLightIntensity;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TMap<EPartOfDay, float> SkyLightIntensity;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TMap<EPartOfDay, float> DirectionalLightTemperature;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float InterpSpeed{0.2};
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnChangeDay(int32 Day);

	UFUNCTION(BlueprintImplementableEvent)
	void OnChangePartOfDay(EPartOfDay PartOfDay);
private:
	UPROPERTY()
	AVikingsGameStateBase* CurrentGameState;
};
