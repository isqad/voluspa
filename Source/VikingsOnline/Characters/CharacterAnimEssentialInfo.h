﻿#pragma once

#include "CoreMinimal.h"
#include <VikingsOnline/Characters/EGait.h>
#include <VikingsOnline/Characters/EMovementState.h>
#include "CharacterAnimEssentialInfo.generated.h"

USTRUCT(BlueprintType)
struct FCharacterAnimEssentialInfo
{
	GENERATED_BODY()

	// Run or Walk
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EGait Gait;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EMovementState MovementState;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector Velocity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector Acceleration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector MovementInput;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsMoving{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool HasMovementInput{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double Speed{ 0.0 };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double MovementInputAmount{ 0.0 };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FRotator AimingRotation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	double AimYawRate{ 0.0 };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsValid{ false };
};
