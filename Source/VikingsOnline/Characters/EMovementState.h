﻿#pragma once

#include "EMovementState.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	GROUNDED = 0 UMETA(DisplayName = "Grounded"),
	INAIR = 1 UMETA(DisplayName = "InAir"),
	MANTLING = 1 UMETA(DisplayName = "Mantling"),
	RAGDOLL = 1 UMETA(DisplayName = "Ragdoll")
};
