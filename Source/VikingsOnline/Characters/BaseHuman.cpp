// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseHuman.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Engine/LocalPlayer.h"
#include "Engine/EngineBaseTypes.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
//#include "ReplicatedActor.h"
#include "Net/UnrealNetwork.h"

#include "GenericPlatform/GenericPlatformMath.h"

#include <VikingsOnline/Utils/VikingsMath.h>
#include <VikingsOnline/Inventory/InventoryComponent.h>
#include <VikingsOnline/Interaction/InteractionComponent.h>
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ABaseHuman::ABaseHuman()
{
	bReplicates = true;
	
	// setting up movement
	bUseControllerRotationYaw = false;
	if (const auto CharMovement = GetCharacterMovement(); IsValid(CharMovement))
	{
		CharMovement->bOrientRotationToMovement = true;
		CharMovement->bUseControllerDesiredRotation = true;
		CharMovement->RotationRate = DefaultRotationRate;
		
		CharMovement->NavAgentProps.bCanCrouch = true;
		CharMovement->NavAgentProps.bCanFly = true;

		// Settings from ALS-Refactored
		CharMovement->MaxAcceleration = 1500.0f;
		CharMovement->BrakingFrictionFactor = 0.0f;
		CharMovement->bRunPhysicsWithNoController = true;

		CharMovement->GroundFriction = 4.0f;
		// GetCharacterMovement()->MaxWalkSpeed = 375.0f;
		CharMovement->MaxWalkSpeedCrouched = 200.0f;
		CharMovement->MinAnalogWalkSpeed = 25.0f;
		CharMovement->bCanWalkOffLedgesWhenCrouching = true;
		CharMovement->PerchRadiusThreshold = 20.0f;
		CharMovement->PerchAdditionalHeight = 0.0f;
		CharMovement->LedgeCheckThreshold = 0.0f;

		CharMovement->AirControl = 0.15f;
		// https://unrealengine.hatenablog.com/entry/2019/01/16/231404
		CharMovement->FallingLateralFriction = 1.0f;
		CharMovement->JumpOffJumpZFactor = 0.0f;
		CharMovement->bNetworkAlwaysReplicateTransformUpdateTimestamp = true; // Required for view network smoothing.

		CharMovement->bAllowPhysicsRotationDuringAnimRootMotion = true; // Used to allow character rotation while rolling.
		
		CharMovement->SetJumpAllowed(true);
        CharMovement->SetNetAddressable(); // Make DSO components net addressable
        CharMovement->SetIsReplicated(true); // Enable replication by default
	}

	check(IsValid(GetRootComponent()));
	
#pragma region CharacterBody
	Head = CreateDefaultSubobject<USkeletalMeshComponent>("Head");
	check(IsValid(Head));
	Head->SetupAttachment(GetRootComponent());

	Torso = CreateDefaultSubobject<USkeletalMeshComponent>("Torso");
	check(IsValid(Torso));
	Torso->SetupAttachment(GetRootComponent());

	Legs = CreateDefaultSubobject<USkeletalMeshComponent>("Legs");
	check(IsValid(Legs));
	Legs->SetupAttachment(GetRootComponent());

	Beard = CreateDefaultSubobject<USkeletalMeshComponent>("Beard");
	check(IsValid(Beard));
	Beard->SetupAttachment(GetRootComponent());

	Mustaches = CreateDefaultSubobject<USkeletalMeshComponent>("Mustaches");
	check(IsValid(Mustaches));
	Mustaches->SetupAttachment(GetRootComponent());
#pragma endregion CharacterBody
	
	// setting up components
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	check(IsValid(SpringArm));

	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->bEnableCameraLag = true;
	SpringArm->TargetArmLength = 150.f;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	check(IsValid(Camera));

	Camera->SetupAttachment(SpringArm);

	Inventory = CreateDefaultSubobject<UInventoryComponent>("InventoryComponent");
	check(IsValid(Inventory));

	Interaction = CreateDefaultSubobject<UInteractionComponent>("InteractionComponent");
	check(IsValid(Interaction));
	
	SpeedScale = BaseWalkScale;

	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseHuman::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseHuman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		CalcEssentialMovementValues();
	}
}

void ABaseHuman::SetupPlayerInputComponent(UInputComponent* Input)
{
	Super::SetupPlayerInputComponent(Input);

	if (UEnhancedInputComponent* EnhancedInput = Cast<UEnhancedInputComponent>(Input); IsValid(EnhancedInput))
	{
		check(LookAction != nullptr);
		check(MoveAction != nullptr);
		check(BlockAction != nullptr);
		check(SprintAction != nullptr);
		check(JumpAction != nullptr);
		check(CrouchAction != nullptr);
		check(AttackAction != nullptr);
		check(InventoryAction != nullptr);
		check(InteractAction != nullptr);

		EnhancedInput->BindAction(LookAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnLook);
		EnhancedInput->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::Input_Move);
		EnhancedInput->BindAction(BlockAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnBlock);
		EnhancedInput->BindAction(SprintAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnSprint);
		EnhancedInput->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnJump);
		EnhancedInput->BindAction(CrouchAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnCrouch);
		EnhancedInput->BindAction(AttackAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnAttack);
		EnhancedInput->BindAction(InventoryAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnInventory);
		EnhancedInput->BindAction(InteractAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnInteract);
	}
}

void ABaseHuman::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	
	// Will be called on client from the server
	AddInputMappingContext();
}

#pragma region Client
void ABaseHuman::AddInputMappingContext_Implementation()
{
	AddInputMappingContextToController();
}

void ABaseHuman::AddInputMappingContextToController() const
{
	if (!IsValid(GetController()))
	{
		return;
	}
	
	const auto CharacterPlayer = Cast<APlayerController>(GetController());
	
	check(IsValid(CharacterPlayer));
	UEnhancedInputLocalPlayerSubsystem* InputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(CharacterPlayer->GetLocalPlayer());

	check(IsValid(InputSubsystem ));
	InputSubsystem->ClearAllMappings();
	check(InputMappingContext != nullptr);
	InputSubsystem->AddMappingContext(InputMappingContext, 0);
}
#pragma endregion Client

void ABaseHuman::Input_OnLook(const FInputActionValue& ActionValue)
{
	const FVector2D Value = ActionValue.Get<FVector2D>();

	AddControllerPitchInput(Value.Y * LookUpMouseSensitivity);
	AddControllerYawInput(Value.X * LookRightMouseSensitivity);
}

void ABaseHuman::Input_Move(const FInputActionValue& ActionValue)
{
	if (!IsValid(GetController()))
	{
		return;
	}

	const FVector2D Value = UVikingsMath::ClampMagnitude012D(ActionValue.Get<FVector2D>());

	const FRotator Rotation = GetController()->GetControlRotation();

	const FVector ForwardDirection = UVikingsMath::AngleToDirection2D(Rotation.Yaw);
	const FVector RightDirection = UVikingsMath::RightVectorFromVector(ForwardDirection);

	AddMovementInput(ForwardDirection * Value.Y + RightDirection * Value.X, SpeedScale);
}

void ABaseHuman::Input_OnBlock(const FInputActionValue& ActionValue)
{
}

// Sprint
void ABaseHuman::Input_OnSprint(const FInputActionValue& ActionValue)
{
	const bool Value = ActionValue.Get<bool>();
	Value ? Server_Sprint() : Server_UnSprint();
}

void ABaseHuman::Server_Sprint_Implementation()
{
	SpeedScale = BaseSprintScale;
	Gait = EGait::RUN;
}

bool ABaseHuman::Server_Sprint_Validate()
{
    return true;
}

void ABaseHuman::Server_UnSprint_Implementation()
{
	SpeedScale = BaseWalkScale;
	Gait = EGait::WALK;
}

bool ABaseHuman::Server_UnSprint_Validate()
{
    return true;
}
// End of Sprint code

// Jump
void ABaseHuman::Input_OnJump(const FInputActionValue& ActionValue)
{
	const auto Value = ActionValue.Get<bool>();
	Value ? Server_Jump() : Server_UnJump();
}

void ABaseHuman::OnRep_Jumping()
{
	// On Client
	bJumping ? Jump() : StopJumping();
}

void ABaseHuman::Server_Jump_Implementation()
{
	bJumping = true;
	
	if (NeedCallClientFunc())
	{
		OnRep_Jumping();
	}
}

bool ABaseHuman::Server_Jump_Validate()
{
    return true;
}

void ABaseHuman::Server_UnJump_Implementation()
{
	bJumping = false;
	if (NeedCallClientFunc())
	{
		OnRep_Jumping();
	}
}

bool ABaseHuman::Server_UnJump_Validate()
{
    return true;
}
// End of Jump

// Crouch
void ABaseHuman::Input_OnCrouch(const FInputActionValue& ActionValue)
{
	if (const auto Value = ActionValue.Get<bool>(); !Value)
	{
		return;
	}

	if (const auto CharMovement = GetCharacterMovement(); CharMovement != nullptr)
	{
		if (!CharMovement->IsCrouching())
		{
			Server_Crouch();
		}
		else
		{
			Server_UnCrouch();
		}
	}
}

void ABaseHuman::OnRep_Crouching()
{
	// On Client
	const auto CharMovement = GetCharacterMovement();
	if (CharMovement == nullptr)
	{
		return;
	}

	if (bCrouching && !CharMovement->IsCrouching())
	{
		Crouch();
		return;
	}

	if (!bCrouching && CharMovement->IsCrouching())
	{
		UnCrouch();
	}
}

void ABaseHuman::CalcEssentialMovementValues()
{
	if (GetWorld() == nullptr || GetCharacterMovement() == nullptr)
	{
		return;
	}
	
	const auto DeltaSeconds = GetWorld()->DeltaTimeSeconds;
	if (DeltaSeconds == 0)
	{
		return;
	}
	const auto ControlRotation = GetControlRotation().Yaw;
	const auto Velocity = GetVelocity();
	const auto CurrentAcceleration = GetCharacterMovement()->GetCurrentAcceleration();

	auto IsInAir = GetCharacterMovement()->IsFalling();
	
	MovementState = IsInAir ? EMovementState::INAIR : EMovementState::GROUNDED;
	
	FCharacterAnimEssentialInfo Info;
	Info.Speed = Velocity.Size2D();
	Info.IsMoving = Info.Speed > 1.0;
	Info.Velocity = Velocity;
	Info.Acceleration = (Velocity - PreviousVelocity) / DeltaSeconds;
	Info.AimYawRate = FMath::Abs((ControlRotation - PreviousAimYaw) / DeltaSeconds);
	Info.MovementInput = CurrentAcceleration;
	Info.AimingRotation = GetControlRotation();
	Info.MovementInputAmount = CurrentAcceleration.Size() / GetCharacterMovement()->GetMaxAcceleration();
	Info.HasMovementInput = Info.MovementInputAmount > 0;
	Info.Gait = Gait;
	Info.MovementState = MovementState;
	Info.IsValid = true;

	PreviousVelocity = Velocity;
	PreviousAimYaw = ControlRotation;

	EssentialMovementValues = Info;
}

void ABaseHuman::Server_Crouch_Implementation()
{
	bCrouching = true;
	if (NeedCallClientFunc())
	{
		OnRep_Crouching();
	}
}

bool ABaseHuman::Server_Crouch_Validate()
{
    return true;
}

void ABaseHuman::Server_UnCrouch_Implementation()
{
	bCrouching = false;
	if (NeedCallClientFunc())
	{
		OnRep_Crouching();
	}
}

bool ABaseHuman::Server_UnCrouch_Validate()
{
    return true;
}
// End of Crouch

void ABaseHuman::Input_OnAttack(const FInputActionValue& ActionValue)
{
}

// Inventory
void ABaseHuman::Input_OnInventory(const FInputActionValue& ActionValue)
{
}

// Interact
void ABaseHuman::Input_OnInteract(const FInputActionValue& ActionValue)
{
}
void ABaseHuman::Server_Interact_Implementation()
{
}
bool ABaseHuman::Server_Interact_Validate()
{
	return true;
}

void ABaseHuman::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ABaseHuman, bJumping, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, Energy, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, Health, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, bCrouching, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, bAlive, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, bSprinting, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ABaseHuman, SpeedScale, COND_OwnerOnly);
	
	DOREPLIFETIME(ABaseHuman, EssentialMovementValues);
	DOREPLIFETIME(ABaseHuman, Gait);
}

bool ABaseHuman::NeedCallClientFunc() const
{
	return GetNetMode() == ENetMode::NM_Standalone || GetNetMode() == ENetMode::NM_ListenServer;
}
