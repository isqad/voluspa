// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "GameFramework/Character.h"
#include "CharacterAnimEssentialInfo.h"
#include "EGait.h"
#include "EMovementState.h"
#include "BaseHuman.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UInputMappingContext;
class UInputAction;
class UInventoryComponent;
class UInteractionComponent;
class USkeletalMeshComponent;
class AHUD;

UCLASS(Blueprintable)
class VIKINGSONLINE_API ABaseHuman : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseHuman();
	
	virtual void Tick(float DeltaTime) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PossessedBy(AController* NewController) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay Options")
	float LookUpMouseSensitivity{ 1.f };
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay Options")
	float LookRightMouseSensitivity{ 1.f };

#pragma region Components
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UCameraComponent> Camera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<USpringArmComponent> SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UInventoryComponent> Inventory;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UInteractionComponent> Interaction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Head;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Torso;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Legs;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Beard;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Mustaches;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	TObjectPtr<USkeletalMeshComponent> Hair;
#pragma endregion Components
	
#pragma region MappingContext
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	TObjectPtr<UInputMappingContext> InputMappingContext;
	
private:
	UFUNCTION(Client, Reliable)
	void AddInputMappingContext();
	
	void AddInputMappingContextToController() const;
#pragma endregion MappingContext

#pragma region InputActions
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> InventoryAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> InteractAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> SprintAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> WalkAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> CrouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> BlockAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> RollAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	TObjectPtr<UInputAction> AttackAction;

private:
	void Input_OnLook(const FInputActionValue& ActionValue);
	void Input_Move(const FInputActionValue& ActionValue);
	void Input_OnBlock(const FInputActionValue& ActionValue);
	void Input_OnSprint(const FInputActionValue& ActionValue);
	void Input_OnJump(const FInputActionValue& ActionValue);
	void Input_OnCrouch(const FInputActionValue& ActionValue);
	void Input_OnAttack(const FInputActionValue& ActionValue);
	void Input_OnInventory(const FInputActionValue& ActionValue);
	void Input_OnInteract(const FInputActionValue& ActionValue);
#pragma endregion InputActions

#pragma region State
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "State | Health")
	bool bAlive{ true };
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "State | Health")
	float Health{ 10.f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State | Health")
	float HealthRegeneration{ 0.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State | Health")
	float MaxHealth{ 0.0f };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "State | Energy")
	float Energy{ 0.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State | Energy")
	float EnergyRegeneration{ 0.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State | Energy")
	float MaxEnergy{ 0.0f };
#pragma endregion State

#pragma region Movement
public:
	const double BaseSprintScale{ 1.f };
	const double BaseWalkScale{ .55f };

	UPROPERTY(Replicated)
	FCharacterAnimEssentialInfo EssentialMovementValues;
protected:	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, ReplicatedUsing=OnRep_Crouching, Category = Movement)
	bool bCrouching{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, ReplicatedUsing=OnRep_Jumping, Category = Movement)
	bool bJumping{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = Movement)
	bool bSprinting{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = Movement)
	double SpeedScale{ BaseWalkScale };
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = Movement)
	EGait Gait{ EGait::WALK };

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = Movement)
	EMovementState MovementState{ EMovementState::GROUNDED };
private:
	const double VelocityMovementLimit{ 3.f };
	
	UFUNCTION(Server, unreliable, WithValidation)
	void Server_Sprint();
	UFUNCTION(Server, unreliable, WithValidation)
	void Server_UnSprint();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_Jump();
	UFUNCTION(Server, unreliable, WithValidation)
	void Server_UnJump();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_Crouch();
	UFUNCTION(Server, unreliable, WithValidation)
	void Server_UnCrouch();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_Interact();

	UFUNCTION()
	void OnRep_Jumping();
	UFUNCTION()
	void OnRep_Crouching();

	// Should be called every tick
	void CalcEssentialMovementValues();
	
	UPROPERTY()
	double PreviousAimYaw{ 0.0 };
	UPROPERTY()
	FVector PreviousVelocity;
#pragma endregion Movement

	bool NeedCallClientFunc() const;
private:
	const FRotator DefaultRotationRate{ 0.f, 500.f, 0.f };
	const FRotator ZeroRotationRate{ 0.f, 0.f, 0.f };
	
	UPROPERTY()
	TObjectPtr<AHUD> CurrentHUD;
};
