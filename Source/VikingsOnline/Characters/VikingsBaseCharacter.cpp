// Fill out your copyright notice in the Description page of Project Settings.


#include "VikingsBaseCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "InputActionValue.h"
#include "Net/UnrealNetwork.h"
#include "GameplayTagContainer.h"

#include "ALS/Public/Utility/AlsMath.h"

#include "../Game/VikingsPlayerController.h"
#include "../Inventory/InventoryComponent.h"
#include "../HUD/PlayerHUD.h"

#include "../Interaction/InteractionComponent.h"
#include "Kismet/KismetMathLibrary.h"

AVikingsBaseCharacter::AVikingsBaseCharacter()
{
	bReplicates = true;
	bUseControllerRotationYaw = false;

	check(IsValid(GetRootComponent()));

	// setting up components
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	check(IsValid(SpringArm));

	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->SetUsingAbsoluteRotation(true);
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 4.f;
	SpringArm->TargetArmLength = 1200.f;
	DesiredCamDistance = SpringArm->TargetArmLength;
	SpringArm->bDoCollisionTest = false;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	check(IsValid(Camera));

	Camera->SetupAttachment(SpringArm);
	Camera->SetFieldOfView(45.f);

	Inventory = CreateDefaultSubobject<UInventoryComponent>("InventoryComponent");
	check(IsValid(Inventory));

	Interaction = CreateDefaultSubobject<UInteractionComponent>("InteractionComponent");
	check(IsValid(Interaction));

	PrimaryActorTick.bCanEverTick = true;
}

void AVikingsBaseCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	SpringArm->TargetArmLength = UKismetMathLibrary::FInterpTo(
		SpringArm->TargetArmLength,
		DesiredCamDistance,
		DeltaSeconds,
		CameraZoomSpeed
		);
}

void AVikingsBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AVikingsBaseCharacter::NotifyControllerChanged()
{
	if (GetNetMode() == NM_DedicatedServer)
	{
		return;
	}
	
	if(const auto PreviousPlayerController = Cast<AVikingsPlayerController>(PreviousController); PreviousPlayerController)
	{
		PreviousPlayerController->OnInputActionMoveDelegate.Clear();
		PreviousPlayerController->OnInputActionScrollDelegate.Clear();
		PreviousPlayerController->OnInputActionInteractDelegate.Clear();
		PreviousPlayerController->OnInputActionJumpDelegate.Clear();
		PreviousPlayerController->OnInputActionInventoryDelegate.Clear();

		Inventory->OnInventoryLoadDelegate.Clear();
		Inventory->OnOpenInventoryDelegate.Clear();

		if(const auto PlayerHUD = Cast<APlayerHUD>(PreviousPlayerController->GetHUD()); PlayerHUD)
		{
			PlayerHUD->OnOverlayModeSelectedDelegate.Clear();
		}
	}

	if(const auto NewPlayerController = Cast<AVikingsPlayerController>(GetController()); NewPlayerController)
	{
		NewPlayerController->OnInputActionInteractDelegate.BindDynamic(this, &ThisClass::Input_OnInteract);
		NewPlayerController->OnInputActionMoveDelegate.BindDynamic(this, &ThisClass::Input_Move);
		NewPlayerController->OnInputActionJumpDelegate.BindDynamic(this, &ThisClass::Input_Jump);
		NewPlayerController->OnInputActionScrollDelegate.BindDynamic(this, &ThisClass::Input_Scroll);
		NewPlayerController->OnInputActionInventoryDelegate.BindDynamic(this, &ThisClass::Input_OnInventory);

		// Debug overlary menu, lock/unlock cam zoom
		NewPlayerController->OnInputActionOverlayMenuDelegate.AddUniqueDynamic(this, &ThisClass::Input_OnDebugOverlayMenu);
		NewPlayerController->OnInputActionOverlayMenuSelectedDelegate.AddUniqueDynamic(this, &ThisClass::Input_OnDebugOverlayMenuSelected);
		
		if(const auto PlayerHUD = Cast<APlayerHUD>(NewPlayerController->GetHUD()); PlayerHUD)
		{
			Inventory->OnInventoryLoadDelegate.AddUniqueDynamic(PlayerHUD, &APlayerHUD::OnInventoryLoad);
			Inventory->OnOpenInventoryDelegate.AddUniqueDynamic(PlayerHUD, &APlayerHUD::OnInventoryOpen);

			PlayerHUD->OnOverlayModeSelectedDelegate.AddUniqueDynamic(this, &ThisClass::OnNewOverlayModeSelected);
		}
		
		Inventory->Load();
	}
	
	Super::NotifyControllerChanged();
}

void AVikingsBaseCharacter::Input_OnLook(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_Move(const FInputActionValue& ActionValue)
{
	const auto Value{UAlsMath::ClampMagnitude012D(ActionValue.Get<FVector2D>())};

	const auto ForwardDirection{UAlsMath::AngleToDirectionXY(UE_REAL_TO_FLOAT(GetViewState().Rotation.Yaw))};
	const auto RightDirection{UAlsMath::PerpendicularCounterClockwiseXY(ForwardDirection)};

	AddMovementInput(ForwardDirection * Value.Y + RightDirection * Value.X);
}

void AVikingsBaseCharacter::Input_Jump(const FInputActionValue& ActionValue)
{
	if (ActionValue.Get<bool>())
	{
		if (StopRagdolling())
		{
			return;
		}

		if (StartMantlingGrounded())
		{
			return;
		}

		if (GetStance() == AlsStanceTags::Crouching)
		{
			SetDesiredStance(AlsStanceTags::Standing);
			return;
		}

		Jump();
	}
	else
	{
		StopJumping();
	}
}

void AVikingsBaseCharacter::Input_OnBlock(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_OnSprint(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_OnJump(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_OnCrouch(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_OnAttack(const FInputActionValue& ActionValue)
{
}

void AVikingsBaseCharacter::Input_OnInventory(const FInputActionValue& ActionValue)
{
	if(Inventory)
	{
		Inventory->OpenInventory();
	}
}

void AVikingsBaseCharacter::Input_OnInteract(const FHitResult& HitResultUnderCursor)
{	
	OnInteract(HitResultUnderCursor);
}

void AVikingsBaseCharacter::OnNewOverlayModeSelected(const FGameplayTag& Overlay)
{
	SetOverlayMode(Overlay);
}

#pragma region Camera
void AVikingsBaseCharacter::Input_OnDebugOverlayMenu()
{
	bLockCameraZoom = true;
}

void AVikingsBaseCharacter::Input_OnDebugOverlayMenuSelected()
{
	bLockCameraZoom = false;
}

void AVikingsBaseCharacter::Input_Scroll(const FInputActionValue& ActionValue)
{
	if (!bLockCameraZoom)
	{
		CameraZoom(ActionValue.Get<float>());
	}
}

void AVikingsBaseCharacter::CameraZoom(const float ScaleFactor)
{
	DesiredCamDistance = UKismetMathLibrary::FClamp(DesiredCamDistance + (-ScrollDistanceScale * ScaleFactor), MinCamDistance, MaxCamDistance);
}
#pragma endregion Camera

void AVikingsBaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}
