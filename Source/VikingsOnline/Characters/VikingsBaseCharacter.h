// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AlsCharacter.h"
#include "VikingsBaseCharacter.generated.h"

class UInventoryComponent;
class UCameraComponent;
class USpringArmComponent;
class UInputMappingContext;
class UInputAction;
class UInventoryComponent;
class UInteractionComponent;
struct FInputActionValue;

/**
 * 
 */
UCLASS()
class VIKINGSONLINE_API AVikingsBaseCharacter : public AAlsCharacter
{
	GENERATED_BODY()
public:
	AVikingsBaseCharacter();

	UPROPERTY(BlueprintReadwrite, VisibleAnywhere, Category=Input)
	bool bInAttacking{false};

	virtual void Tick(float DeltaSeconds) override;
	
	virtual void BeginPlay() override;

	virtual void NotifyControllerChanged() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

#pragma region Components
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UCameraComponent> Camera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<USpringArmComponent> SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UInventoryComponent> Inventory;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	TObjectPtr<UInteractionComponent> Interaction;
#pragma endregion Components

#pragma region Control
public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category=Control)
	bool bRotationLocked{false};
#pragma endregion Control

#pragma region InputActions
private:
	UFUNCTION()
	void Input_Move(const FInputActionValue& ActionValue);
	UFUNCTION()
	void Input_Jump(const FInputActionValue& ActionValue);
	UFUNCTION()
	void Input_Scroll(const FInputActionValue& ActionValue);
	UFUNCTION()
	void Input_OnInventory(const FInputActionValue& ActionValue);
	UFUNCTION()
	void Input_OnInteract(const FHitResult& HitResultUnderCursor);

	UFUNCTION()
	void Input_OnDebugOverlayMenu();
	UFUNCTION()
	void Input_OnDebugOverlayMenuSelected();
	
	void Input_OnLook(const FInputActionValue& ActionValue);
	void Input_OnBlock(const FInputActionValue& ActionValue);
	void Input_OnSprint(const FInputActionValue& ActionValue);
	void Input_OnJump(const FInputActionValue& ActionValue);
	void Input_OnCrouch(const FInputActionValue& ActionValue);
	void Input_OnAttack(const FInputActionValue& ActionValue);
	
#pragma endregion InputActions

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void OnInteract(const FHitResult& HitResultUnderCursor);
	UFUNCTION()
	void OnNewOverlayModeSelected(const FGameplayTag& Overlay);

#pragma region Camera
protected:
	UPROPERTY(EditAnywhere, Category=Camera)
	float ScrollDistanceScale{25.f};

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category=Camera)
	bool bLockCameraZoom{false};

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category=Camera)
	float CameraZoomSpeed{2.f};
	
private:
	const float MinCamDistance{400.f};
	const float MaxCamDistance{2300.f};
	
	UPROPERTY()
	float DesiredCamDistance;

	void CameraZoom(const float ScaleFactor);
#pragma endregion Camera
};
