﻿#pragma once

#include "EGait.generated.h"

UENUM(BlueprintType)
enum class EGait : uint8
{
	WALK = 0 UMETA(DisplayName = "Walk"),
	RUN = 1 UMETA(DisplayName = "Run")
};
