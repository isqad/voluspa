// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/IntPoint.h"
#include "InventoryItem.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

// Через эти делегаты UI может подписаться на нужные ему события
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInventoryLoad, const FIntPoint&, Dimensions);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenInventory);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VIKINGSONLINE_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UInventoryComponent();
	
	UFUNCTION(Server, unreliable, WithValidation)
	void OpenInventory();

	UFUNCTION(Server, unreliable, WithValidation)
	void Load();

	UPROPERTY()
	FOnOpenInventory OnOpenInventoryDelegate;

	UPROPERTY()
	FOnInventoryLoad OnInventoryLoadDelegate;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Rows{6};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Columns{12};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing=OnRep_InventoryDimensionChanged)
	FIntPoint Dimensions{};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	TArray<FInventoryItem> Items{};

private:
	UPROPERTY()
	bool bLoaded{ false };
	
	UPROPERTY(ReplicatedUsing=OnRep_InventoryOpened)
	bool bInventoryOpened{ false };
	
	UFUNCTION()
	void OnRep_InventoryOpened() const;

	UFUNCTION()
	void OnRep_InventoryDimensionChanged() const;

	bool IsStandalone() const;
};
