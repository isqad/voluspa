#include "InventoryComponent.h"
#include "Net/UnrealNetwork.h"

UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UInventoryComponent::Load_Implementation()
{
	if (bLoaded)
	{
		return;
	}
	
	Dimensions = {Columns, Rows};
	if (IsStandalone())
	{
		OnRep_InventoryDimensionChanged();
	}
	// TODO: load from saves
	Items.Init(FInventoryItem{}, Dimensions.X * Dimensions.Y - 1);

	bLoaded = true;
}

bool UInventoryComponent::Load_Validate()
{
	return true;
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UInventoryComponent::OpenInventory_Implementation()
{
	bInventoryOpened = !bInventoryOpened;
	if (IsStandalone())
	{
		OnRep_InventoryOpened();
	}
}

// Called on Client
void UInventoryComponent::OnRep_InventoryOpened() const
{
	OnOpenInventoryDelegate.Broadcast();	
}

// Called on Client
void UInventoryComponent::OnRep_InventoryDimensionChanged() const
{
	OnInventoryLoadDelegate.Broadcast(Dimensions);
}

bool UInventoryComponent::IsStandalone() const
{
	return GetNetMode() == NM_Standalone || GetNetMode() == NM_ListenServer;
}

bool UInventoryComponent::OpenInventory_Validate()
{
    return true;
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(UInventoryComponent, Dimensions, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UInventoryComponent, Items, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UInventoryComponent, bInventoryOpened, COND_OwnerOnly);
}
