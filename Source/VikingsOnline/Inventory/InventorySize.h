#pragma once

#include "CoreMinimal.h"
#include "InventorySize.generated.h"

USTRUCT(BlueprintType)
struct FInventorySize
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Columns{ 2 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Rows{ 2 };
};
