#pragma once

#include "InventoryItemInterface.generated.h"

UINTERFACE(BlueprintType)
class VIKINGSONLINE_API UInventoryItemInterface : public UInterface 
{
    GENERATED_BODY()
};

class IInventoryItemInterface
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    const FName GetInventoryName();
};
