#pragma once

#include "InventoryItem.generated.h"

USTRUCT(BlueprintType)
struct VIKINGSONLINE_API FInventoryItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UClass* ItemClass{ nullptr };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Count{ 0 };
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanStacked{ false };
};
