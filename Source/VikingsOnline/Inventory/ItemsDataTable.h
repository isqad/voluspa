#pragma once

#include "CoreMinimal.h"
#include "ItemsDataTable.generated.h"

USTRUCT(BlueprintType)
struct FItemsDataTable : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UClass* ItemClass{ nullptr };

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSoftObjectPtr<UTexture> InventoryIcon;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanStacked{ false };
};
