#pragma once

#include "CoreMinimal.h"
#include <VikingsOnline/Interaction/Interfaces/InteractiveItemInterface.h>
#include "BaseItem.generated.h"

UCLASS(Blueprintable)
class VIKINGSONLINE_API ABaseItem : public AActor, public IInteractiveItemInterface
{
	GENERATED_BODY()

public:
    ABaseItem();
    /**
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FName GetUIInteractText(); virtual FName GetUIInteractText_Implementation() override;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Interact(); virtual void Interact_Implementation() override;
    */
};
