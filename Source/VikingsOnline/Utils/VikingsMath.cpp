// Fill out your copyright notice in the Description page of Project Settings.


#include "VikingsMath.h"
//#include "Kismet/KismetMathLibrary.h"

inline FVector2D UVikingsMath::ClampMagnitude012D(const FVector2D& Vector)
{
	const double MagnitudeSquared = Vector.SizeSquared();

	if (MagnitudeSquared <= 1.0f)
	{
		return Vector;
	}

	const double Scale = FMath::InvSqrt(MagnitudeSquared);

	return { Vector.X * Scale, Vector.Y * Scale };
}

inline FVector UVikingsMath::RadianToDirection3D(const float Radian)
{
	float Sin, Cos;
	FMath::SinCos(&Sin, &Cos, Radian);

	return { Cos, Sin, 0.0f };
}

inline FVector UVikingsMath::AngleToDirection2D(const float Angle)
{
	return UVikingsMath::RadianToDirection3D(FMath::DegreesToRadians(Angle));
}

inline FVector UVikingsMath::RightVectorFromVector(const FVector& Vector)
{
	return { -Vector.Y, Vector.X, Vector.Z };
}
