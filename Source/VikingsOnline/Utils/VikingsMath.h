// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "VikingsMath.generated.h"

/**
 * Some usefull math
 */
UCLASS()
class VIKINGSONLINE_API UVikingsMath : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "Vikings Math|Vector", DisplayName = "Clamp Magnitude 01 2D",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector2D ClampMagnitude012D(const FVector2D& Vector);

	UFUNCTION(BlueprintPure, Category = "Vikings Math|Vector", DisplayName = "Radian To Direction 3D",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector RadianToDirection3D(const float Radian);

	UFUNCTION(BlueprintPure, Category = "Vikings Math|Vector", DisplayName = "Angle To Direction 2D",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector AngleToDirection2D(const float Angle);

	UFUNCTION(BlueprintPure, Category = "Vikings Math|Vector", DisplayName = "Right Vector From Vector",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector RightVectorFromVector(const FVector& Vector);
};
