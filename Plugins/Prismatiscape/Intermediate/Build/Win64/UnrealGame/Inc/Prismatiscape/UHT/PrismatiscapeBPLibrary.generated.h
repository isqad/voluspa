// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PrismatiscapeBPLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRISMATISCAPE_PrismatiscapeBPLibrary_generated_h
#error "PrismatiscapeBPLibrary.generated.h already included, missing '#pragma once' in PrismatiscapeBPLibrary.h"
#endif
#define PRISMATISCAPE_PrismatiscapeBPLibrary_generated_h

#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_SPARSE_DATA
#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPrismatiscapeSampleFunction);


#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_ACCESSORS
#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUPrismatiscapeBPLibrary(); \
	friend struct Z_Construct_UClass_UPrismatiscapeBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UPrismatiscapeBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Prismatiscape"), NO_API) \
	DECLARE_SERIALIZER(UPrismatiscapeBPLibrary)


#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPrismatiscapeBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPrismatiscapeBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPrismatiscapeBPLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPrismatiscapeBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPrismatiscapeBPLibrary(UPrismatiscapeBPLibrary&&); \
	NO_API UPrismatiscapeBPLibrary(const UPrismatiscapeBPLibrary&); \
public: \
	NO_API virtual ~UPrismatiscapeBPLibrary();


#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_25_PROLOG
#define FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_SPARSE_DATA \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_RPC_WRAPPERS \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_ACCESSORS \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_INCLASS \
	FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRISMATISCAPE_API UClass* StaticClass<class UPrismatiscapeBPLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Unreal_Projects_PrismaticaGameUE5_Plugins_Prismatiscape_Source_Prismatiscape_Public_PrismatiscapeBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
